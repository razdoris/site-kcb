<?php $title = "KCB-S'inscrire" ?>
<?php ob_start(); ?>  
	<div class="chapitre" id="sinscrire">
        <h2 class="h2View">Inscription en ligne</h2>
        <h3 class="h3View">Saison <span id="firstYear">2020</span>-2021</h3>
        <div class="category categorySmall" id="categoryInformation">
                    <h4>INFORMATION</h4>
                    <div class="typeRadio fieldGroup fieldGroup50 fieldGroupNewKarate fieldRequired">
                        <label for="fieldNewKarate" class="fieldLabel">1<sup>ere</sup> année de Karaté?</label>
                        <div class="fieldSubGroupe">
                            <span class="fieldOption">
                                <input type="radio" value="oui" id="fieldNewKarate0" name="fieldNewKarate" required="required" aria-required="true" autofocus="autofocus"> 
                                <label for="fieldNewKarate0">oui</label>
                            </span>
                            <span class="fieldOption">
                                <input type="radio" value="non" id="fieldNewKarate1" name="fieldNewKarate" required="required" aria-required="true"> <label for="fieldNewKarate01">non</label>
                            </span>
                        </div>
                    </div>
                    <div class="typeText fieldGroup fieldGroup50 fieldGroupNumLicence">
                        <label for="fieldNumLicence" class="fieldLabel">N° Licence (si connu)</label>
                        <input size="10" type="text" name="fieldNumLicence" id="fieldNumLicence" class="fieldText">
                    </div>
                    <div class="typeText fieldGroup fieldGroup50 fieldGroupGrade">
                        <label for="fieldGrade" class="fieldLabel">Grade</label>
                        <select name="fieldGrade" id="fieldGrade" class="fieldSelect" aria-required="true">
                            <option value="">choisir dans la liste</option>
                            <optgroup class="white" label="6ème Kyu:">
                                <option class="white" value="blanche"> - blanche</option>
                                <option class="white" value="blancheJaunes"> - blanche-jaune</option>
                            </optgroup>
                            <optgroup class="yellow" label="5ème Kyu:">
                                <option class="yellow" value="jaune">- jaune</option> 
                                <option class="yellow" value="jauneOrange"> - jaune-orange</option>
                            </optgroup>
                            <optgroup class="orange" label="4ème Kyu:">
                                <option class="orange" value="orange">- orange</option>
                                <option class="orange" value="orangeVert"> - orange-vert</option>
                            </optgroup>
                            <optgroup class="green" label="3ème Kyu:">
                                <option class="green" value="vert">- vert</option>
                            </optgroup>
                            <optgroup class="blue" label="2ème Kyu:">
                                <option class="blue" value="bleu">- bleu</option>
                            </optgroup>
                            <optgroup class="brown" label="1er Kyu:">
                                <option class="brown" value="marron">- marron</option>
                            </optgroup>
                            <optgroup class="black" label="Dan:">
                            <option class="black" value="noir1">- noir 1ere</option>
                            <option class="black" value="noir2">- noir 2eme</option>
                            <option class="black" value="superieur">- superieur</option>
                        </select>
                    </div>
                    <div class="typeRadio fieldGroup fieldGroup100 fieldGroupCompetition fieldRequired">
                        <label for="fieldCompetition" class="fieldLabel">Compétition</label>
                        <div class="fieldSubGroupe">
                            <div class="subCombatLeft">
                                <span class="fieldOption">
                                    <input type="radio" value="non" id="fieldCompetition0" name="fieldcompetition" required="required" aria-required="true"> 
                                    <label for="fieldCompetition0">non</label>
                                </span>
                                <span class="fieldOption">
                                    <input type="radio" value="oui" id="fieldCompetition1" name="fieldcompetition" required="required" aria-required="true"> 
                                    <label for="fieldcompetition">oui :</label>
                                </span>
                            </div>
                            <div class="subCombatRight">
                                <span class="fieldOption">
                                    <input type="checkbox" value="kata" class="fieldCompetition" id="fieldCompetitionKata"> 
                                    <label for="fieldCompetitionKata">Kata</label>
                                </span>
                                <span class="fieldOption">
                                    <input type="checkbox" value="combat" class="fieldCompetition" id="fieldCompetitionCombat"> 
                                    <label for="fieldcompetition">Combat</label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="category categorySmall" id="categoryIdentity">
                    <h4>ETAT CIVIL</h4>
                    <div class="typeText fieldGroup fieldGroup100 fieldGroupNom fieldRequired">
                         <label for="fieldNom" class="fieldLabel">Nom</label>
                         <input size="10" type="text" name="fieldNom" id="fieldNom" class="fieldText" required="required" aria-required="true">	
                    </div>
                    <div class="typeText fieldGroup fieldGroup100 fieldGroupPrenom fieldRequired">
                         <label for="fieldPrenom" class="fieldLabel">Prénom</label>
                         <input size="10" type="text" name="fieldPrenom" id="fieldPrenom" class="fieldText" required="required" aria-required="true">
                    </div>
                    <div class="typeRadio fieldGroup fieldGroup100 fieldGroupSexe fieldRequired">
                            <label for="fieldSexe" class="fieldLabel">Sexe</label>
                            <div class="fieldSubGroupe">
                                <span class="fieldOption">
                                    <input type="radio" value="H" id="fieldSexe0" name="fieldSexe" required="required" aria-required="true"> 
                                    <label for="fieldSexe0">Masculin</label>
                                </span>
                                <span class="fieldOption">
                                    <input type="radio" value="F" id="fieldSexe1" name="fieldSexe" required="required" aria-required="true"> 
                                    <label for="fieldSexe1">Feminin</label>
                                </span>
                            </div>
                    </div>
                    <div class="fieldTypeDate fieldGroup fieldGroup50 fieldGroupBirthday fieldRequired">
                        <label for="fieldBirthday" class="fieldLabel">Date de naissance</label>
                        <input type="date" name="fieldBirthday" id="fieldBirthday" class="fieldBirthday" required="required" aria-required="true" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">	
                    </div>
                    <div class="fieldTypeSelect fieldGroup fieldGroup50 fieldGroupCategory fieldRequired">
                        <label for="fieldCategory" class="fieldLabel">Catégorie d'âge</label>
                        <div class="field">
                            <select name="fieldCategory" id="fieldCategory" class="fieldSelect" required="required" aria-required="true">
                                <option value="">choisir dans la liste</option>
                                <option value="Baby">Babys/Mini poussins (Né(e) en 2015)</option>
                                <option value="M7">Poussins (Né(e) en 2014 ou 2013)</option>
                                <option value="M9">Pupilles (Né(e) en 2012 ou 2011)</option>
                                <option value="M11">Benjamins (Né(e) en 2010 ou 2009)</option>
                                <option value="M13">Minimes (Né(e) en 2008 ou 2007)</option>
                                <option value="M15">Cadets (Né(e) en 2006 ou 2005)</option>
                                <option value="M17">Juniors (Né(e) en 2004 ou 2003)</option>
                                <option value="M20">Espoirs (Né(e) en 2002, 2001 ou 2000)</option>
                                <option value="Senior">Senior (né(e) entre 1999 et 1986)</option>
                                <option value="Vétéran">Vétéran (né(e) en 1985 et avant)</option>
                            </select>
                        </div>
                    </div>
                    <div class="typeText fieldGroup fieldGroup100 fieldGroupNationalite fieldRequired">
                        <label for="fieldNationalite" class="fieldLabel">Nationalité</label>
                        <input size="10" type="text" name="fieldNationalite" value="Française" id="fieldNationalite" class="fieldText" required="required" aria-required="true">
                    </div>
                    <div class="fieldGroup fieldGroup100 fieldGroupAdress  fieldRequired">
                        <div class="groupTitle">Adresse (des parents pour les adhérents mineurs))</div>
                        <div class="typeTextArea fieldSubGroup fieldGroup100 fieldGroupStreet">
                            <label for="FieldAdress" class="fieldLabel">N° et rue</label>
                            <textarea class="fieldText" name="FieldAdress" id="FieldAdress" rows="1" placeholder="Adresse complète" required="required" aria-required="true"></textarea>
                        </div>
                        <div class="typeNumber fieldSubGroup fieldGroup30 fieldGroupCodePostal fieldRequired">
                            <label for="fieldCodePostal" class="fieldLabel">Code postal</label>
                            <input size="1" type="number" name="fieldCodePostal" id="fieldCodePostal" class="fieldNumber" required="required" aria-required="true" min="" max="">	
                        </div>
                        <div class="typeText fieldSubGroup fieldGroup60 fieldGroupVille fieldRequired">
                            <label for="fieldVille" class="fieldLabel">Ville</label>
                            <input size="10" type="text" name="fieldVille" id="fieldVille" class="fieldText" placeholder="Ville" required="required" aria-required="true">
                        </div>
                    </div>
                    <div class="TypeEmail fieldGroup fieldGroup100 fieldGroupEmail fieldRequired">
                        <label for="fieldEmail" class="fieldLabel">E-mail</label>
                        <input size="10" type="email" name="fieldEmail" id="fieldEmail" class="fieldEMail" placeholder="E-mail" required="required" aria-required="true">
                    </div>
                    <div class="fieldGroup fieldGroup100 fieldGroupPhone fieldRequired">
                        <div class="groupTitle">Téléphone</div>
                        <div class="typePhone fieldSubGroup fieldGroupMobilePhone fieldGroup50">
                            <label for="fieldMobilePhone" class="fieldLabel">portable</label>
                            <input size="10" type="tel" name="fieldMobilePhone" id="fieldMobilePhone" class="fieldPhone" pattern="[0-9()#&amp;+*-=.]+" title="Seuls les caractères de numéros de téléphone (#, -, *, etc.) sont acceptés.">
                        </div>
                        <div class="typePhone fieldSubGroup fieldGroupFixePhone fieldGroup50">
                            <label for="fieldFixePhone" class="fieldLabel">fixe</label>
                            <input size="10" type="tel" name="fieldFixePhone" id="fieldFixePhone" class="fieldPhone" pattern="[0-9()#&amp;+*-=.]+" title="Seuls les caractères de numéros de téléphone (#, -, *, etc.) sont acceptés.">
                        </div>
                    </div>
                </div>
                <div class="category categorySmall" id="categoryEmergency">
                    <H4>EN CAS D'URGENCE</H4>
                    <div class="fieldGroup fieldGroup100 fieldGroupEmergency fieldRequired">
                        <div class="groupTitle">Personne à contacter</div>
                        <div class="typeText fieldGroupIdentityICE fieldSubGroup fieldGroup100 fieldRequired">
                            <label for="fieldIdentityICE" class="fieldLabel">Nom Prenom</label>
                            <input size="10" type="text" name="fieldIdentiteICE" id="fieldIdentiteICE" class="fieldText" required="required" aria-required="true">	
                        </div>
                        <div class="typePhone fieldSubGroup fieldGroup100 fieldGroupPhoneICE fieldRequired">
                            <label for="fieldPhoneICE" class="fieldLabel">téléphone (portable de préference)</label>
                            <input size="10" type="tel" name="fieldPhoneICE" id="fieldPhoneICE" class="fieldPhone" pattern="[0-9()#&amp;+*-=.]+" title="Seuls les caractères de numéros de téléphone (#, -, *, etc.) sont acceptés.">
                        </div>
                    </div>
                </div>
                <div class="category categoryBig" id="categoryAutorisation">
                    <H4>AUTORISATION</H4>
                    <div class="typeInfo fieldGroup fieldGroup100 parentalAdvisory fieldGroupeParentalAutorisation">
                        je soussigné(e) 
                        <input size="20" type="text" name="fieldParentalIdentity" id="fieldParentalIdentity" class="fieldInfo"> autorise 
                        <select name="fieldFiliation" id="fieldFiliation" class="fieldFiliation">
                                <option value="fils">mon fils</option>
                                <option value="fille">ma fille</option>
                        </select> 
                        à pratiquer le karaté <br /> au « Karaté Club de Besné » pour la saison 2020-2021.
                    </div>
                    <div class="typeValidate fieldGroup fieldGroup100 parentalAdvisory fieldGroupTransport">
                        <input type="checkbox" class="fieldCheck" name="fieldTransport" id="fieldTransport">
                        <label for="fieldTransport" class="LabelCheckbox">
                            <p>En cochant cette case j'autorise mon enfant a etre transporté surles lieux de stages ou de compétitions (<strong>si et seulement si </strong>je ne peux pas le conduire par mes propres moyens ce qui est préferable.)</p>
                        </label>
                    </div>
                    <div class="typeValidate fieldGroup fieldGroup100 fieldGroupImage">
                        <input type="checkbox" class="fieldCheck" name="fieldImage" id="fieldImage">
                        <label for="fieldImage" class="LabelCheckbox">
                            <p>En cochant cette case l'adhérent ou son representant légal autorise le Karaté Club de Besné à utiliser son image sur tout support afin de promouvoir  le club, ses activités et ses resultats, à l'exclusion de toute utilisation commercial</p>
                        </label>
                    </div>
                </div>
                <div class="category categoryBig" id="categoryAssurance">
                    <H4>ASSURANCE</H4>
                    <div class="typeValidate fieldGroup fieldGroup100 fieldGroupImage">                        
                        <div class="typeInfo fieldLabelAssurance">je soussigné déclare</div>
                        <input type="checkbox" value="oui" name="fieldAssurance" id="fieldAssurance0" class="checkboxAssurance">
                        <label for="fieldAssurance" class="fieldLabelInline LabelAssurance">
                            <ul class="">
                                <li class="liAssurance"><strong>adhérer à l’assurance</strong>  « garanties de base accidents corporels » proposée par la FFK.</li>
                                <li class="liAssurance"><strong>Régler</strong> la somme de 37 € TTC (licence : 36,25 € TTC et assurance : 0,75 € TTC - compris dans le prix de votre adhésion au club).</li>
                                <li class="liAssurance"><strong>Accepter</strong> que mes données personnelles recueillies fassent l’objet d’un traitement informatique par la FFK</li>
                                <li class="liAssurance"><strong>Avoir pris connaissance, sur ffkarate.fr, des informations relatives :</strong>aux assurances et garanties complémentaires, à la validité de la licence, au certificat médical et à la loi du 06 janvier 1978 modifiée « Informatique et libertés ».</li>
                            </ul>
                        </label>
                        <!--
                        <input type="radio" value="non" name="fieldAssurance" id="fieldAssurance1" class="radioAssurance">
                        <label for="fieldAssurance" class="fieldLabelInline">
                            <ul class="LabelAssurance">
                                <li><strong>Refuser d’adhérer à l’assurance</strong>   « garanties de base accidents corporels » proposée par la FFK ; dans ce cas, <strong> le soussigné reconnait avoir été informé des risques encourus par la pratique du Karaté et des Disciplines associées.</strong></li>
                                <li><strong>Accepter</strong>que mes données personnelles recueillies fassent l’objet d’un traitement informatique par la FFK</li>
                                <li><strong>Avoir pris connaissance, sur ffkarate.fr, des informations relatives :</strong>aux assurances et garanties complémentaires, à la validité de la licence, au certificat médical et à la loi du 06 janvier 1978 modifiée « Informatique et libertés ».</li>
                            </ul>
                        </label>
                        -->
                    </div>
                </div>
                <div class="categoryBig">
                    <div class="buttonGroup">
                        <button class="fieldButton" id="validate">valider</button><button class="fieldButton" id="cancelled">annuler</button>
                    </div>
                </div>
	</div>


<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/presentation.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/inscription.css" media="screen"/>
<script src="public/js/jquery.min.js"></script>
<script src="public/js/inscription.js"></script> 
<?php require('template.php'); ?>