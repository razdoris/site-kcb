<?php 
$title = "KCB-Login"; 
$siteKey = G_SITE_KEY;
ob_start(); ?>
<title><?= $title ?></title>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
   function onSubmit(token) {
     document.getElementById("formIdentificate").submit();
   }
</script>
<section class="textPresentation">
    <p>Merci de vous connecter</p>
</section>
<section class="sectionMsgAlert">
    <p class="msgAlert"><?= $msg ?></p>
</section>
<section id="formIdentification">
    <form id="formIdentificate" action="./index.php?view=<?= $currentView ?>" method="post">
        <h3 class="titleIdentification">IDENTIFICATION</h3>
        <div class="fieldGroup fieldLogin" id="fieldLogin">
            <label for="inputLogin" class="fieldLabel" id="labelLogin">identifiant</label>
            <br />
            <input type="text" class="fieldInput" id="inputLogin" placeholder="identifiant" autocomplete="username"  name="login"/>
        </div>
        <div class="fieldGroup fieldPwd" id="fieldPwd">
            <label for="inputPwd" class="fieldLabel" id="labelPwd" >mot de passe</label>
            <br />
            <input type="password" class="fieldInput" id="inputPwd" placeholder="password" autocomplete="current-password"  name="pwd"/>
        </div>
        <div class="fieldGroup fieldButton" id="buttonValide">
            <!--<button class="buttons" type="submit">valider</button>-->
            <button class="g-recaptcha buttons" data-sitekey="<?= $siteKey ?>" data-callback='onSubmit' data-action='submit'  type="submit">valider</button>
        </div>
        
    </form>

</section>

<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" href="public/css/login.css" media="screen"/>
<script src="public/js/jquery.min.js"></script>
<script src="public/js/accueil.js"></script>
<?php require('template.php'); ?>