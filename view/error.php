<?php 
$pageTitle = "Error";
$title = "ACPA " . $pageTitle;  ?>
<?php ob_start(); ?>  
	<div class="mainMenu">
        <h2><?= $pageTitle ?></h2>
		<?= $errorMsg ?>
	</div>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>