<?php $title = "KCB-Accueil" ?>
<?php ob_start(); ?>  
	<div class="mainMenu">
        <div class="linePaveLien">
            <span href="index.php?view=presentation" class="paveLien">
                <span class="labelpaveLien">Le club
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./index.php?view=presentation">Presentation</a></li>
                            <li class="listePave"><a href="./index.php?view=presentation#adress">Adresse</a></li>
                            <li class="listePave"><a href="./index.php?view=presentation#horaire">Horaires d'Entrainements</a></li>
                        </ul>
                    </span>
                </span>	
                <span class="logoPaveLien">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-file-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v10.755S4 11 8 11s5 1.755 5 1.755V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>
                      <path fill-rule="evenodd" d="M8 10a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>
                </span>
            </span>
            <span href="index.php?view=presentation" class="paveLien">
                <span class="labelpaveLien">Actualités
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./index.php?view=blog">le blog du club</a></li>
                            <li class="listePave2"><a href="https://www.cdkarate44.fr/">Site du CDK44</a></li>
                            <li class="listePave2"><a href="https://www.mairie-besne.fr/">Site de la mairie</a></li>
                        </ul>
                    </span>
                </span>	
                <span class="logoPaveLien">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-newspaper" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5v-11zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5H12z"/>
                      <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
                    </svg>
                </span>
            </span>
        </div>
        <div class="linePaveLien">
            <span href="index.php?view=inscription" class="paveLien">
                <span class="labelpaveLien">Inscription
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./index.php?view=sinscrire">Via Formulaire</a></li>
                            <li class="listePave notEnable"><a href="./index.php">En Ligne</a></li>
                        </ul>
                    </span>
                </span>
                <span class="logoPaveLien">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                    </svg>
                </span>
            </span>
            <span href="index.php?view=lesson" class="paveLien">
                <span class="labelpaveLien">Leçons
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./index.php?view=ceinture">Par Niveau</a></li>
                            <li class="listePave"><a href="./index.php?view=abckcb">L'abc du kcb</a></li>
                            <li class="listePave"><a href="./index.php?view=video">Vidéo</a></li>
                        </ul>
                    </span>
                </span>
                <span class="logoPaveLien">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-collection-play" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M14.5 13.5h-13A.5.5 0 0 1 1 13V6a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5zm-13 1A1.5 1.5 0 0 1 0 13V6a1.5 1.5 0 0 1 1.5-1.5h13A1.5 1.5 0 0 1 16 6v7a1.5 1.5 0 0 1-1.5 1.5h-13zM2 3a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 0-1h-11A.5.5 0 0 0 2 3zm2-2a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 0-1h-7A.5.5 0 0 0 4 1z"/>
                      <path fill-rule="evenodd" d="M6.258 6.563a.5.5 0 0 1 .507.013l4 2.5a.5.5 0 0 1 0 .848l-4 2.5A.5.5 0 0 1 6 12V7a.5.5 0 0 1 .258-.437z"/>
                    </svg>
                </span>	
            </span>
            <span href="index.php?view=jeux" class="paveLien">
                <span class="labelpaveLien">Jeux
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./index.php?view=quiz">Quiz</a></li>
                            <li class="listePave"><a href="./index.php?view=parades">Parades</a></li>
                        </ul>
                    </span>
                </span>
                <span class="logoPaveLien">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-joystick" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path d="M7.106 15.553L.553 12.276A1 1 0 0 1 0 11.382V9.471a1 1 0 0 1 .606-.89L6 6.269v1.088L1 9.5l5.658 2.83a3 3 0 0 0 2.684 0L15 9.5l-5-2.143V6.27l5.394 2.312a1 1 0 0 1 .606.89v1.911a1 1 0 0 1-.553.894l-6.553 3.277a2 2 0 0 1-1.788 0z"/>
                      <path fill-rule="evenodd" d="M7.5 9.5v-6h1v6h-1z"/>
                      <path d="M10 9.75c0 .414-.895.75-2 .75s-2-.336-2-.75S6.895 9 8 9s2 .336 2 .75zM10 2a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>
                    </svg>
                </span>
            </span>
        </div>
        <div class="linePaveLien">
            <span href="index.php?view=presentation" class="paveLien">
                <span class="labelpaveLien">Photos
                    <span class="petitLabel">
                        <ul class="grpListePave">
                            <li class="listePave"><a href="./view/photos/galerie">Galerie</a></li>
                        </ul>
                    </span>
                </span>
                <span class="logoPaveLien">
                    <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-camera" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M15 12V6a1 1 0 0 0-1-1h-1.172a3 3 0 0 1-2.12-.879l-.83-.828A1 1 0 0 0 9.173 3H6.828a1 1 0 0 0-.707.293l-.828.828A3 3 0 0 1 3.172 5H2a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z"/>
  <path fill-rule="evenodd" d="M8 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
  <path d="M3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
</svg>
                </span>	
            </span>
        </div>
	</div>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/accueil.css" media="screen"/>
<?php require('template.php'); ?>