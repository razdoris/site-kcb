<?php $title = "KCB-abckcb" ?>
<?php ob_start(); ?>
<div class="title"><h2>L'alphabet du KCB</h2></div>
<div class="airJeux">
    <div class="explication">
        <p>Utilise le ou les mots qui s'affichent pour composer ton entrainement!!</p>
        <p>A chaque lettre du mot correspond une activités physique, réalise les dans l'ordre.</p>
        <p>Tu peux changer de mot en cliquant dessus</p>
    </div>
    <div id="randmot">
        <span id="motRandom">motrandom</span>
    </div>
    <div class="lettre">
        <h4>A</h4>
        <span>Fait 10 zenkutsu<br>
            <span class="activiteNiv2">(avance une jambe pour te mettre en zenkutsu, puis revient en YOÏ)</span>
        </span>
    </div>
    <div class="lettre">
        <h4>B</h4>
        <span>Gainage ventral pendant 10 secondes</span>
    </div>
    <div class="lettre">
        <h4>C</h4>
        <span>Kokutsu sur chaque jambe <br>pendant 10 secondes</span>
    </div>
    <div class="lettre">
        <h4>D</h4>
        <span>Gainage lateral sur le bras gauche<br>
            <span class="activiteNiv2">le bras droit est tendu en l'air</span>
        </span>
    </div>
    <div class="lettre">
        <h4>E</h4>
        <span>Fait 20 Tsuki rapide</span>
    </div>
    <div class="lettre">
        <h4>F</h4>
        <span>Saute en faisant demi tour 3 fois</span>
    </div>
    <div class="lettre">
        <h4>G</h4>
        <span>Fait 10 jumping Jack Flash</span>
    </div>
    <div class="lettre">
        <h4>H</h4>
        <span>Fait 10 abdominaux<br>
            <span class="activiteNiv2">jambe tendu, bras sur les cuisses, lever les épaules</span>
        </span>
    </div>
    <div class="lettre">
        <h4>I</h4>
        <span>Gainage lateral sur le bras droit<br>
            <span class="activiteNiv2">le bras gauche est tendu en l'air</span>
        </span>
    </div>
    <div class="lettre">
        <h4>J</h4>
        <span>Fait 10 Gedan Barai</span>
    </div>
    <div class="lettre">
        <h4>K</h4>
        <span>Fait 10 monter des genoux</span>
    </div>
    <div class="lettre">
        <h4>L</h4>
        <span>Fait 5 squats</span>
    </div>
    <div class="lettre">
        <h4>M</h4>
        <span>Fait 10 sauts en extension</span>
    </div>
    <div class="lettre">
        <h4>N</h4>
        <span>Fait 10 Uchi Uke</span>
    </div>
    <div class="lettre">
        <h4>O</h4>
        <span>fait 10 abdominaux<br>
            <span class="activiteNiv2">allongé sur le dos jambes pliés, tend une jambe et le bras opposé</span>
        </span>
    </div>
    <div class="lettre">
        <h4>P</h4>
        <span>Fait 10 Mae Geri</span>
    </div>
    <div class="lettre">
        <h4>Q</h4>
        <span>Fait 10 talons-fesse sur place</span>
    </div>
    <div class="lettre">
        <h4>R</h4>
        <span>Fait 10 Jodan Age Uke</span>
    </div>
    <div class="lettre">
        <h4>S</h4>
        <span>Mets toi debout sur une jambe et essai de tenir 10s<br>
            <span class="activiteNiv2">(change de jambe)</span>
        </span>
    </div>
    <div class="lettre">
        <h4>T</h4>
        <span>Kiba Dachi pendant 10 secondes</span>
    </div>
    <div class="lettre">
        <h4>U</h4>
        <span>Fait 20 Tsuki rapide</span>
    </div>
    <div class="lettre">
        <h4>V</h4>
        <span>Fait 10 Yoko Geri</span>
    </div>
    <div class="lettre">
        <h4>W</h4>
        <span>fait 10 Gyaku Tsuki en alternant<br>
            <span class="activiteNiv2">avec retour en YOÏ entre chaque coup</span>
        </span>
    </div>
    <div class="lettre">
        <h4>X</h4>
        <span>Fait 10 Kizami Tsuki<br>
            <span class="activiteNiv2">Met toi en position de combat et fait 5 Kizami puis change de garde et recommence</span>
        </span>
    </div>
    <div class="lettre">
        <h4>Y</h4>
        <span>Fait 10 Mountain Climber<br>
            <span class="activiteNiv2">en position gainage avance les genoux sous la poitrine</span>
        </span>
    </div>
    <div class="lettre">
        <h4>Z</h4>
        <span>Mets toi accroupi et releve toi en sautant 5 fois</span>
    </div>
</div>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/abckcb.css" media="screen"/> 
<script src="public/js/jquery.min.js"></script>
<script src="public/js/abckcb.js"></script> 
<?php require('view/template.php'); ?>