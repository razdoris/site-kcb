<?php $title = "KCB-Lesson par ceinture" ?>
<?php ob_start(); ?>  
	<div class="chapitre" id="leClub">
        <h2 class="h2View">Programme par niveau</h2>
        <div class="selectionNiveau">
            <label for="choixCeinture" class="labelChoix">Séléctionner la ceinture que vous ciblez:</label>
            <select id="choixCeinture" class="selectChoix">
                <optgroup class="yellow" label="5ème Kyu:">
                    <option class="yellow" value="jaune" 
                       <?php 
                            if($colorBelt=='jaune'){
                                ?>selected<?php
                            }
                        ?>
                            >- jaune</option> 
                </optgroup>
                <optgroup class="oranges" label="4ème Kyu:">
                    <option class="oranges" value="orange"
                       <?php 
                            if($colorBelt=='orange'){
                                ?>selected<?php
                            }
                        ?>
                            >- orange</option>
                </optgroup>
                <optgroup class="green" label="3ème Kyu:">
                    <option class="green" value="vert"
                       <?php 
                            if($colorBelt=='vert'){
                                ?>selected<?php
                            }
                        ?>
                            >- vert</option>
                </optgroup>
                <optgroup class="blue" label="2ème Kyu:">
                    <option class="blue" value="bleu"
                       <?php 
                            if($colorBelt=='bleu'){
                                ?>selected<?php
                            }
                        ?>
                            >- bleu</option>
                </optgroup>
                <optgroup class="brown" label="1er Kyu:">
                    <option class="brown" value="marron" 
                       <?php 
                            if($colorBelt=='marron'){
                                ?>selected<?php
                            }
                        ?>
                            >- marron</option>
                </optgroup>
            </select>
        </div>
        <div class="conteneurImageProg">
            <img class="imageProg" src="./public/images/cours/jaune.jpg">
        </div>
        <div class="tableauCoup">
            <table>
                <thead>
                    <tr>
                        <th class="withBorder">Positions - Dachi</th>
                        <th class="withBorder">Parades - Uke</th>
                        <th class="withBorder">Pieds - Geri</th>
                        <th >Mains - Tsuki</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="withBorder jaune">Zenkutsu Dachi</td>
                        <td class="withBorder jaune">Gedan Baraï</td>
                        <td class="withBorder jaune">Mae Geri</td>
                        <td class="jaune">Oi Tsuki</td>
                    </tr>
                    <tr>
                        <td class="withBorder jaune">Kokutsu Dachi</td>
                        <td class="withBorder jaune">Age Uke</td>
                        <td class="withBorder jaune">Mawashi Geri</td>
                        <td class="jaune">Gyaku Tsuki</td>
                    </tr>
                    <tr>
                        <td class="withBorder orange">Kiba Dachi</td>
                        <td class="withBorder jaune">Uchi Uke</td>
                        <td class="withBorder orange">Yoko Geri</td>
                        <td class="orange">Nukité</td>
                    </tr>
                    <tr>
                        <td class="withBorder marron">Kosa Dachi</td>
                        <td class="withBorder jaune">Shuto Uke</td>
                        <td class="withBorder vert">ushiro Geri</td>
                        <td class="vert">Uraken</td>
                    </tr>
                    <tr>
                        <td class="withBorder marron">Neko Achi Dachi</td>
                        <td class="withBorder orange">Soto Uke</td>
                        <td class="withBorder vert">Ura Mawashi Geri</td>
                        <td class="vert">Kisami Tsuki</td>
                    </tr>
                    <tr>
                        <td class="withBorder"></td>
                        <td class="withBorder bleu">Haishu Uke</td>
                        <td class="withBorder bleu">Mikazuki Geri</td>
                        <td class="vert">Maïté Tsuki</td>
                    </tr>
                    <tr>
                        <td class="withBorder"></td>
                        <td class="withBorder"></td>
                        <td class="withBorder"></td>
                        <td class="withBorder bleu">Taisho</td>
                    </tr>
                    <tr>
                        <td class="withBorder"></td>
                        <td class="withBorder"></td>
                        <td class="withBorder"></td>
                        <td class="bleu">Aito</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="autresAcquis"><h3>Savoir complémentaire:</h3>
            <ul>
                <li class="listNiv1 vert">Randori: savoir
                    <ul>
                        <li class="listNiv2">Attaquer</li>
                        <li class="listNiv2">Parer</li>
                        <li class="listNiv2">Contre attaquer</li>
                        <li class="listNiv2">Enchainer lors d'une attaque</li>
                    </ul>
                </li>
                <li class="listNiv1 bleu">Travail des cibles</li>
                <li class="listNiv1 marron">Multi directionnel</li> 
                <li class="listNiv1 marron">Sambon &#x26; Gohon Kumité</li>                
            </ul>
        </div>
    </div>
    
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/ceinture.css" media="screen"/>  
<script src="public/js/jquery.min.js"></script>
<script src="public/js/ceinture.js"></script> 
<?php require('view/template.php'); ?>