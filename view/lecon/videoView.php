<?php 

$title = "KCB-Video" ;
ob_start(); 
echo "<div class='title'><h2>Cours Video</h2></div>
        <div class='sectionExplication'>
        <h3>Avant Propos</h3>
        <p>Ces vidéos sont faites par le KCB, pour le KCB, par conséquent il est interdit de les télécharger, ou de les partagés sur les reseaux sociaux.</p>
        <p>Le but de ces vidéos est de montrer les mouvements aux licenciés. Les positions ne sont pas parfaites mais nous pourront corriger cela dès le retour sur le tatamis.</p>
        <br>
        <p>Pour lancer/stopper la vidéo, cliquer dessus.</p>
        <br>
        <p>Les enfants qui sont sur ces vidéos, ont accepté de les faire pour aider les membres du KCB. Merci de ne pas vous moquer d\'eux et de les respecter</p>
        <p>Pour les adultes vous pouvez vous moquer tant qu'il n'y à pas de méchanceté</p>
        <p class='redText'>Tous manquement à ces règles entrainera la suppression des vidéos du site</p>
        </div><div class='masterSectionVid'>";

$videoDirectory = './public/video';

$totCode="";
if($videoDir = opendir($videoDirectory))
{
    while(false !== ($videoSuDirectory = readdir($videoDir)))
    {
        if($videoSuDirectory != '.' && $videoSuDirectory != '..' && $videoSuDirectory != 'index.php')
        {
            $code = '<div class="sectionVid" id="'.$videoSuDirectory.'">
                    <h3 id="00'.substr($videoSuDirectory,3).'" class=" class'.substr($videoSuDirectory,3).'">'. substr($videoSuDirectory,3) .'</h3>';
            $videoSubDirectoryComplet = $videoDirectory .'/' . $videoSuDirectory;
            if($videoSubDir = opendir($videoSubDirectoryComplet))
            {
                while(false !== ($videoKarate = readdir($videoSubDir)))
                {
                    if($videoKarate != '.' && $videoKarate != '..' && $videoKarate != 'index.php')
                    {
                        $code = $code . 
                            '<div id="'.$videoKarate.'" class="contenerVideo class'.substr($videoSuDirectory,3).'">';
                        if(strpos($videoKarate, 'png') == FALSE )
                        {
                            $code =$code . '<video class="videoCours" width="250" muted loop>
                                    <source src="' . $videoSubDirectoryComplet .'/' . $videoKarate .'"  type="video/mp4" >
                                    Vidéo non supporté par votre navigateur
                                </video>
                                <h4>'. substr($videoKarate,3,-4) .'</h4>';
                        }else{
                            $code =$code . '<img style="width:250px" class="videoCours" src="' . $videoSubDirectoryComplet .'/' . $videoKarate .'"  />
                                <h4>'. substr($videoKarate,4,-4) .'</h4>';
                        }                        
                        $code =$code . '</div>';
                        
                       
                    }
                }
            }
            $code = $code . '</div>';
            $totCode = $code . $totCode . "</div>";
        }
    }
}
echo $totCode;
echo '<div class="sectionKata" id="ZZ Kata">
        <h3>Kata (vidéo issue de youtube)</h3>
            <div class="contenerVideo">
                <iframe width="250" height="315"  src="https://www.youtube.com/embed/9D2yOzDsW8k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Heian Shodan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315"  src="https://www.youtube.com/embed/6Hc1NMdjU9U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Heian Nidan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315"  src="https://www.youtube.com/embed/8OI-FTV3-4o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Heian Sandan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315"  src="https://www.youtube.com/embed/HrAiTCcatbY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Heian Yondan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315"  src="https://www.youtube.com/embed/dBFe54glhTs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Heian Godan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/tXPZFarJMh0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Bassai Dai</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/VAkA5zAosC4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Tekki Shodan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/Jkv8Ks_fEqk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Kanku Dai</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/SW5907Eeo7c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Empi</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/u4Hw9nipExg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Jitte</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/Lrr-c5RCKpo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Jion</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/9uqw0g3E4BY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Tekki Nidan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/2VLz6zksKvk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Bassai Sho</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/52cfzR0S6rU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Hangetsu</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/R1KqSlnu4JE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Kanku Sho</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/-bmF_F2bDoc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Tekki Sandan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/4HobC6dj8jA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Gankaku</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/ou14EWbogB0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Chinte</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/RiG93fNAmz0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Ji\'in</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/jjD5ZqUEeJo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Sochin</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/MDbZdXCdEVE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Meikyo</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/efxiTKgiXS8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Nijushiho</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/py_aMWc4HDo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Wankan</h4>
            </div>
            <div class="contenerVideo">
                <iframe width="250" height="315" src="https://www.youtube.com/embed/X-Dq32yOvSY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4>Unsu</h4>
            </div>
            
        </div>';
$content = ob_get_clean(); 
echo "<link rel='stylesheet' type='text/css' href='public/css/video.css' media='screen'/>
<script src='public/js/jquery.min.js'></script>
<script src='public/js/video.js'></script> ";

require('view/template.php');