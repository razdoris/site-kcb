<?php
$pageTitle = "Quizz";
$title = "KCB " . $pageTitle; 
session_start();
ob_start(); ?>


<div id="tete"><h2> Quiz: Connais-tu le Karaté</h2></div>
<form>
    <div class="question">
        <h3>Question 1:</h3>
        <div class="conteneurImg">
            <img id="img1" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Que signifie Karaté ?</h4>
            <div>
                <input type="radio" id="r1a" name="q1" class="good">Main vide
            </div>
            <div>
                <input type="radio" id="r1b" name="q1">Baston
            </div>
            <div>
                <input type="radio" id="r1c" name="q1">Combat des Samouraï
            </div>            
        </div> 
        
        <div class="reponse" id="reponse1">Le Karate-do peut etre traduit de plusieurs façons:<br> 
        la voie de la main vide, la voie de la main et du vide (voie de la vacuité réalisé par les mains) 
        ou combat à mains nues.<br> La bonne réponses est donc la 1er propositions</div>
    </div>
    <div class="question">
        <h3>Question 2:</h3>
        <div class="conteneurImg">
            <img id="img2" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Quel type de Karaté pratique t'on au KCB ?</h4>
            <div>
                <input type="radio" id="r2a" name="q2" class="good">Shotokan-Ryu
            </div>
            <div>
                <input type="radio" id="r2b" name="q2">Gojo-Ryu
            </div>
            <div>
                <input type="radio" id="r2c" name="q2">Shito-Ryu
            </div>
        </div> 
        <div class="reponse" id="reponse2">Le KCB est spécialisé dans le karaté Shotokan.</div>
    </div>
    <div class="question">
        <h3>Question 3:</h3>  
        <div class="conteneurImg">
            <img id="img3" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Que signifie KCB ?</h4>
            <div>
                <input type="radio" id="r3a" name="q3">Kayak Club de Besné
            </div>
            <div>
                <input type="radio" id="r3b" name="q3" class="good">Karaté Club de Besné
            </div>
            <div>
                <input type="radio" id="r3c" name="q3">Conservatoire Royal de Bruxelles (Koninklijk Conservatorium Brussel)
            </div>
        </div> 
        <div class="reponse" id="reponse3">Le KCB est Le Karaté Club de Besné.<br> 
        le Conservatoire Royal de Bruxelles existe bien également mais cela ne nous concernes pas actuellement.</div>
        </div>
    <div class="question">
        <h3>Question 4:</h3>  
        <div class="conteneurImg">
            <img id="img4" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">            
            <h4>Quel est le logo du Kcb (passe la souris sur une image pour l'agrandir) ?</h4>
            <div>
                <input type="radio" id="r4a" name="q4" class="forImg"><img id="imgReponse1" class="imgReponse" src="./public/images/quizz/logo1.png" />
            </div>
            <div>
                <input type="radio" id="r4b" name="q4" class="forImg"><img id="imgReponse2" class="imgReponse" src="./public/images/quizz/Logo2.png" />
            </div>
            <div>
                <input type="radio" id="r4c" name="q4" class="good forImg"><img id="imgReponse3" class="imgReponse" src="./public/images/quizz/logo3.png" />
            </div>
        </div>
        <div class="reponse" id="reponse4">Le bon logo est le 3eme.<br> 
        Le premier est celui du karate Shotokan, le dernier est celui de la federation française de karaté</div>
        </div>
    <div class="question">
        <h3>Question 5:</h3>
        <div class="conteneurImg">
            <img id="img5" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Quel mot Japonais annonce le départ/commencement d'un exercice ou d'un combat?</h4>
            <div>
                <input type="radio" id="r5a" name="q5" class="good">Hadjimé
            </div>
            <div>
                <input type="radio" id="r5b" name="q5">top à la vachette
            </div>
            <div>
                <input type="radio" id="r5c" name="q5">Mawashi
            </div>
        </div>  
        <div class="reponse" id="reponse5">L'ordre pour commencé est hadjimé, un Mawashi et un coup de pied circulaire. <br>
        Top à la vachette annonce le depart d'un autre type de sport que seul les plus anciens connaissent</div>
        </div>
    <div class="question">
        <h3>Question 6:</h3>
        <div class="conteneurImg">
            <img id="img6" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Comment s'appelle l'inventeur du Karaté shotokan ?</h4>
            <div>
                <input type="radio" id="r6a" name="q6">Maitre Shōzō Kawasaki 
            </div>
            <div>
                <input type="radio" id="r6b" name="q6" class="good">Maitre Gichin Funakoshi
            </div>
            <div>
                <input type="radio" id="r6c" name="q6">Maitre Splinter
            </div>
        </div> 
        <div class="reponse" id="reponse6">Shomen Gichin Funakoshi est considéré comme le père du karaté moderne et créateur du style Shōtōkan.<br>
        Son portrait est affiché dans le dojo, c'est lui que l'on salut en premier.</div>
        </div>
    <div class="question">
        <h3>Question 7:</h3>
        <div class="conteneurImg">
            <img id="img7" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">            
            <h4>Quels sont les ordres à suivre pour le salut traditionnel ?</h4>
            <div>
                <input type="radio" id="r7a" name="q7" class="good">Seiza; Mokusô; Mokusô-Yame; Shomen-ni-rei; Seinsei-ni-rei; O Taga-ni-re; Kiritsu
            </div>
            <div>
                <input type="radio" id="r7b" name="q7">Il n'y a pas de regle particulière
            </div>
            <div>
                <input type="radio" id="r7c" name="q7">Sandan; Nidan; Shodan; Yondan; Godan
            </div>
        </div>   
        <div class="reponse" id="reponse7">Les éleves rentrent dans le dojo, Ils saluent le tatamis en entrant, 
        puis s'alignent par ordre des ceintures.
        <br>Le plus gradé (le sempai) fait assoirent ses camarades en disant "seiza", 
        puis il annonce Mokusô qui annonce le debut d'une meditation (un retour au calme) et mokusô-yame pour mettre fin a cette méditation. 
        <br>Ensuite il ordonne les differents salut: 
            <ul>
                <li>Shomen-ni-rei pour le fondateur du karaté (Gichin Funakoshi en Shotokan);</li>
                <li>Seinsei-ni-rei pour le Senseï (le professeur);</li>
                <li>Otagani-ni-re pour ses camarades;</li>
            </ul>
        A chaques salut les éleves repondent par osu. Enfin le sempai fait se relever les élèves en annoncant "kiritsu".</div>
    </div>
    <div class="question">
        <h3>Question 8:</h3>
        <div class="conteneurImg">
            <img id="img8" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">            
            <h4>Dans quel ordres passe-t-on les ceintures ?</h4>
            <div>
                <input type="radio" id="r8a" name="q8">Blanc, Gris, Noir
            </div>
            <div>
                <input type="radio" id="r8b" name="q8">rouge, orange, jaune, vert, bleu, indigo et violet
            </div>
            <div>
                <input type="radio" id="r8c" name="q8" class="good">Blanche, Jaunes, Orange, Vert, Bleu, Marron, Noir
            </div>
        </div> 
        <div class="reponse" id="reponse8">l'ordre des ceintures est le suivant: 
        Blanche, Jaunes, Orange, Vert, Bleu, Marron, Noir. il existe des ceintures intermediaires mais elles ne sont pas liées à un grade.</div>
    </div>
    <div class="question">
        <h3>Question 9:</h3>
        <div class="conteneurImg">
            <img id="img9" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">            
            <h4>Qu'est-ce qu'un Mae Geri ?</h4>
            <div>
                <input type="radio" id="r9a" name="q9" class="good">Un coup de pied frontal direct
            </div>
            <div>
                <input type="radio" id="r9b" name="q9">Un coup de poing
            </div>
            <div>
                <input type="radio" id="r9c" name="q9">une parade basse
            </div>
        </div>   
        <div class="reponse" id="reponse9">Le Mae Geri est un coup de pied frontal direct.</div>
    </div>    
    <div class="question">
        <h3>Question 10:</h3>
        <div class="conteneurImg">
            <img id="img10" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">
            <h4>Comment appelle-t-on un coup de poing contraire à la jambe qui avance ?</h4>
            <div>
                <input type="radio" id="r10a" name="q10">un coup de poing contraire à la jambe qui avance
            </div>
            <div>
                <input type="radio" id="r10b" name="q10" class="good">Un Gyaku Tsuki
            </div>
            <div>
                <input type="radio" id="r10c" name="q10">un Kizami Tsuki
            </div>
        </div>  
        <div class="reponse" id="reponse10">bien que la première reponse ne soit pas fausse, la reponse attendu est un Gyaku Tsuki.
        Un kizami Tsuki est un coup de poing avec le poing avant.</div>
    </div>    
    <div class="question">
        <h3>Question 11:</h3>
        <div class="texte">            
            <h4>Quel mot ajoute-t-on au nom d'une attaque pour signaler qu'on va la faire avec le membre antérieur (celui qui est devant) ?</h4>
            <div>
                <input type="radio" id="r11a" name="q11" class="good">Kizami
            </div>
            <div>
                <input type="radio" id="r11b" name="q11">Wasabi
            </div>
            <div>
                <input type="radio" id="r11c" name="q11">Ura
            </div>
        </div> 
        <div class="conteneurImg">
            <img id="img11" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="reponse" id="reponse11">Kizami: 
        lorsque l'on frappe en coup de pied direct avec la jambe avant on fait un Kizami Mae Geri</div>
    </div>    
    <div class="question">
        <h3>Question 12:</h3>  
        <div class="conteneurImg">
            <img id="img12" class="icon" src="./public/images/quizz/question.png" />
        </div>
        <div class="texte">            
            <h4>Comment appelle-t-on le "professeur" de Karate ?</h4>
            <div>
                <input type="radio" id="r12a" name="q12" >Maitre
            </div>
            <div>
                <input type="radio" id="r12b" name="q12">Sempai
            </div>
            <div>
                <input type="radio" id="r12c" name="q12"class="good">Sensei
            </div>
        </div> 
        <div class="reponse" id="reponse12">Le "professeur" s'appele un sensei</div>
    </div>
    <button type="button" id="button" class="boutton">Resultat</button>
</form>

<?php $content = ob_get_clean(); ?>

 
<link rel="stylesheet" type="text/css" href="public/css/quizz.css" media="screen"/>   
<script src="public/js/jquery.min.js"></script>
<script src="public/js/quizz.js"></script> 
<?php require('view/template.php'); ?>