<?php
$pageTitle = "parades";
$title = "KCB " . $pageTitle; 
session_start();
ob_start(); ?>

<div id="airedejeux">
    <h2 class="title">LE JEUX DES PARADES</h2>
    <div id="compteur">Prochain coup dans 10s</div>    
    <div id="gardecombat">
        <img id="garde" src="./public/images/parades/Garde.jpg" />
    </div>
    <div id="coup">coup</div>
    <div id="commande">
        <div id="contenu">
            <button type="button" class="button" onClick="redirec()">Hajime</button>
            <button type="button" class="button" onClick="stop()">Yame</button>
        </div>
    </div>
</div>
<div id="notice">
    <h2 class="title2">COMMENT JOUER</h2>
    <div class="explication">
        <p>Ce jeu correspond au Kihon ippon-kumite :<br/>
            Votre adversaire virtuel annonce une attaque, la réalise en vous devez la parer.
            </p>
            <p>
            Attention l'addversaire peut changer de garde avant de frapper.</p>
        <p>Pour démarrer cliquer sur "Hajime"
        <br>
        pour arreter cliquer sur "Yame"</p>
    </div>
</div>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/parades.css" media="screen"/>   
<script src="public/js/jquery.min.js"></script>
<script src="public/js/parades.js"></script> 
<?php require('view/template.php'); ?>