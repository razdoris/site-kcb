<?php $title = "KCB-S'inscrire" ?>
<?php ob_start(); ?>  
	<div class="chapitre" id="leClub">
        <h2 class="h2View">S'inscrire</h2>
        
        <p>Les inscriptions au Karaté Club de Besné sont ouvertes toutes l'année et les démarches sont très simple</p>
        <p>Il suffit de fournir à l'un des membres du bureau (cf. page <a href="./index.php?view=presentation#bureau">bureau</a>) les documents suivants:</p>        
        <ul>
            <li class="listeValeur">Une bulletin d'adhesion à télécharger <a target="_blank" href="../public/images/formulaire%20adh%C3%A9sion.pdf" download="Formulaire d'adhesion.pdf">ici</a></li>
            <li class="listeValeur">Une formulaire "demande de licence par internet" à télécharger <a target="_blank" href="https://www.ffkarate.fr/wp-content/uploads/2020/08/FORMULAIRE-DEMANDE-DE-LICENCE-2020-2021-INTERNET.pdf">ici</a></li>
            <li class="listeValeur">Un certificat médical autorisant la pratique du karaté et de la compétiton ou passeport</li>
            <li class="listeValeur">Une photo d'identité</li>
            <li class="listeValeur">Un chèque à l'ordre du karaté club de besné d'un montant de:
                <ul>
                    <li  class="listeValeur">100€ pour les babys (moins de 6 an)</li>
                    <li  class="listeValeur">130€ pour les enfants (moins de 14 an)</li>
                    <li  class="listeValeur">150€ pour les adultes (moins de 14 an)</li>
                </ul>
            </li>
        </ul>
        <p>En cas de doutes, ou de questions, n'hesitez pas à venir à notre rencontre</p>
	</div>


<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/presentation.css" media="screen"/>
<?php require('template.php'); ?>