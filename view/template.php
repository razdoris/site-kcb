<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-MQZCD6567S"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-MQZCD6567S');
        </script>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="site du karaté club de besné" />
		<meta author="nbeurel" />
		<meta name="keywords" content="karaté club, besné" />
		<title><?= $title ?></title>
		
		
		<link rel="stylesheet" href="public/css/template.css" media="screen"/>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
        <link rel="icon" type="images/ico" href="./public/images/kcb.ico"/>
		
    </head>
	<body>
		<header>
            <div class="burger">
                <span class="switchNav">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </span>
            </div>
			<div class="titleHeader">
				<h1><a class="lienTitle" href='./index.php'><i>K</i>arate <i>C</i>lub de <i>B</i>esne</a></h1>  
            </div>
            <div class="headerPrincipal">
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                  <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                  <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                </svg>
                <div class="headerChild  headerHide">
                    <ul class="headerUl">
                        <?php
                        if(isset($_SESSION['login']) &&  !($_SESSION['login']) == "")
                        {
                        ?>
                        <li class="headerIl"><a href="#">Mon compte</a></li>
                        <li class="headerIl"><a href="./index.php?view=logout">Deconnexion</a></li>
                        <?php
                        }else{
                        ?>
                        <li class="headerIl"><a href="./index.php?view=login">Connexion</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
		</header>        
		<nav class='navHide'>
            <div id="navButton0" class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-file-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v10.755S4 11 8 11s5 1.755 5 1.755V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>
                          <path fill-rule="evenodd" d="M8 10a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
					   <span class="navText">Presentation</span>
                    </span>
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#leClub" >Le club</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#adress">L'adresse</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#horaires">Les horaires</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#bureau">Le bureau</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#enseignant">Les enseignants</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=presentation#historique">L'histoire</a></li>
                        </ul>
                    </span>
				</span>
			</div>	
            <div id="navButton1" class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-newspaper" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5v-11zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5H12z"/>
                          <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
                        </svg>
					   <span class="navText">Actualités</span>
                    </span>
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="./index.php?view=blog">Bolg du club</a></li>
                            <li><a class="navLink niv1" href="https://www.cdkarate44.fr/">Site du CDK44</a></li>
                            <li><a class="navLink niv1" href="https://www.mairie-besne.fr/">Site de la mairie</a></li>
                        </ul>
                    </span>
				</span>
			</div>			
			<div id="navButton2" class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
					   <span class="navText">Inscription</span>
                    </span>
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="./index.php?view=sinscrire" >comment s'inscrire</a></li>
                            <li><a class="navLink niv1 navNotEnable" href="" title="non disponible">inscription en ligne</a></li>
                        </ul>
                    </span>
				</span>
			</div>
			<div id="navButton3" class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-collection-play" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M14.5 13.5h-13A.5.5 0 0 1 1 13V6a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5zm-13 1A1.5 1.5 0 0 1 0 13V6a1.5 1.5 0 0 1 1.5-1.5h13A1.5 1.5 0 0 1 16 6v7a1.5 1.5 0 0 1-1.5 1.5h-13zM2 3a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 0-1h-11A.5.5 0 0 0 2 3zm2-2a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 0-1h-7A.5.5 0 0 0 4 1z"/>
                          <path fill-rule="evenodd" d="M6.258 6.563a.5.5 0 0 1 .507.013l4 2.5a.5.5 0 0 1 0 .848l-4 2.5A.5.5 0 0 1 6 12V7a.5.5 0 0 1 .258-.437z"/>
                        </svg>
                        <span class="navText">Leçon</span>
                    </span>					
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="" >Programme par ceinture</a>
                                <ul>
                                    <li>
                                        <a class="navLink niv2" href="./index.php?view=ceinture&color=jaune" title="non disponible">Jaune
                                            <span class="symboleCeinture ceintureJaune">&#127895;</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="navLink niv2" href="./index.php?view=ceinture&color=orange" title="non disponible">Orange
                                            <span class="symboleCeinture ceintureOrange">&#127895;</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="navLink niv2" href="./index.php?view=ceinture&color=vert" title="non disponible">Vert
                                            <span class="symboleCeinture ceintureVert">&#127895;</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="navLink niv2" href="./index.php?view=ceinture&color=bleu" title="non disponible">Bleu
                                            <span class="symboleCeinture ceintureBleu">&#127895;</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="navLink niv2" href="./index.php?view=ceinture&color=marron" title="non disponible">Marron
                                            <span class="symboleCeinture ceintureMarron">&#127895;</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="navLink niv1" href="./index.php?view=abckcb" >L'abc du kcb</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=video" title="non disponible">vidéo en ligne</a></li>
                        </ul>
                    </span>
				</span>
			</div>
			<div id="navButton4"  class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-joystick" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path d="M7.106 15.553L.553 12.276A1 1 0 0 1 0 11.382V9.471a1 1 0 0 1 .606-.89L6 6.269v1.088L1 9.5l5.658 2.83a3 3 0 0 0 2.684 0L15 9.5l-5-2.143V6.27l5.394 2.312a1 1 0 0 1 .606.89v1.911a1 1 0 0 1-.553.894l-6.553 3.277a2 2 0 0 1-1.788 0z"/>
                          <path fill-rule="evenodd" d="M7.5 9.5v-6h1v6h-1z"/>
                          <path d="M10 9.75c0 .414-.895.75-2 .75s-2-.336-2-.75S6.895 9 8 9s2 .336 2 .75zM10 2a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>
                        </svg>
					   <span class="navText">Jeux</span>
                    </span>
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="./index.php?view=quiz" >Quiz</a></li>
                            <li><a class="navLink niv1" href="./index.php?view=parades" >Jeux des Parades</a></li>
                        </ul>
                    </span>
				</span>
			</div>
            <?php
            if(isset($_SESSION['login']) &&  !($_SESSION['login']) == "")
            {
			?><div id="navButton5"  class="navButton">
				<span href="#" class="navPrinc">
                    <span class="navLogo">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-tools" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M0 1l1-1 3.081 2.2a1 1 0 0 1 .419.815v.07a1 1 0 0 0 .293.708L10.5 9.5l.914-.305a1 1 0 0 1 1.023.242l3.356 3.356a1 1 0 0 1 0 1.414l-1.586 1.586a1 1 0 0 1-1.414 0l-3.356-3.356a1 1 0 0 1-.242-1.023L9.5 10.5 3.793 4.793a1 1 0 0 0-.707-.293h-.071a1 1 0 0 1-.814-.419L0 1zm11.354 9.646a.5.5 0 0 0-.708.708l3 3a.5.5 0 0 0 .708-.708l-3-3z"/>
                          <path fill-rule="evenodd" d="M15.898 2.223a3.003 3.003 0 0 1-3.679 3.674L5.878 12.15a3 3 0 1 1-2.027-2.027l6.252-6.341A3 3 0 0 1 13.778.1l-2.142 2.142L12 4l1.757.364 2.141-2.141zm-13.37 9.019L3.001 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026z"/>
                        </svg>
					   <span class="navText">Parametres</span> 
                    </span>
                    <span class="navChild">
                        <ul>
                            <li><a class="navLink niv1" href="./index.php?view=addArticle" >ajouter un article au blog</a></li>
                        </ul>
                    </span>
				</span>
			</div><?php
            }
            ?>
            <div class="pub"><a href="">offert par razdoris</a></div>
		</nav>
		<main id="corps" >		
			<?= $content ?>
		</main>			
		
		
		<script src="public/js/jquery.min.js"></script>
		<script src="public/js/accueil.js"></script>
		

	</body>
</html>
