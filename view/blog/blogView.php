<?php $title = "KCB-Blog" ?>
<?php ob_start(); ?>

<div class="chapitre" id="blog">
    <h2 class="h2View">Actualités</h2>
    <div class="category"><h3>categories:</h3>
            <select id="selectCat">
                <option class="oneCategory" value='0'>--</option>
    <?php
    while ($cat = $listCategories->fetch())
    {
        ?>
        <option class="oneCategory" value='<?= $cat['id_categories'] ?>' title='<?= $cat['Description_categories'] ?>'><?= $cat['Nom_categories'] ?></option>
    <?php
    } ?>
                </select>
    </div>
    <div id="listArticle">
    <?php
    while ($article = $listArticle->fetch())
    {
        ?>
        <a class="articleContent" href="./index.php?view=article&idArticle=<?= $article['id_article']?>">
            <h3 class="h3View"><?= $article['Title_article']?></h3>
            <div class="resumeArticle"><?= $article['Resume_article']?></div>
        </a>
        <?php
    } ?>
    </div>
    
    <div class="keyWords">
    <?php
    while ($keyWord = $listKeyWords->fetch())
    {
        ?>
        <span class="oneKeyWord" style="font-size:<?= $keyWord['Iteration_motcle'] ?>px" title="<?= $keyWord['Description_motCle'] ?>"><?= $keyWord['Mot_motCle'] ?></span>
        <span class="idKeyWord" ><?= $keyWord['id_motCle'] ?></span>
    <?php
    } ?>
    </div>
</div>
<?php
$content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/accueil.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/blog.css" media="screen"/>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="public/js/jquery.min.js"></script>
<script src="public/js/blogAdd.js"></script>
<?php require('view/template.php'); ?>