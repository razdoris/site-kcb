<?php
$title = "KCB-modifier un article";
ob_start();

while ($article = $listArticle->fetch())
{
    $idArticle = $article['id_article'];
    $titleArticle = $article['Title_article'];        
    $resumeArticle = $article['Resume_article'];
    $contenuArticle = $article['Contenu_article'];
    $categorieArticle = $article['id_categories_articles'];
    $motCle = $article['grp_id_motCle'];
    
}
?>

<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<div >
	<div class="group">
		<input type='text' value="<?= $titleArticle ." " .$categorieArticle ?>" id="title" name="title" />
	</div>
	<div class="group">
		<div id="resume" class="textZone">
		  <?= $resumeArticle ?>
		</div>
	</div>
	<div class="group">
		<div id="article" class="textZone">
		  <?= $contenuArticle ?>
		</div>
	</div>
    <div class="group grpCategory">
        <h5 class="h5Category">categories </h5>
		<?php
        while ($cat = $listCategories->fetch())
        {
            $idCategories = $cat['id_categories'];
            if($idCategories==$categorieArticle){
        ?>
            
        
            <div class="inputCat">
                <input type="radio" class="listCat" name="listCat" value="<?= $idCategories ?>" id="listCat<?= $idCategories ?>" checked >
                <label for="listCat<?= $idCategories ?>" class="catWord"><?= $cat['Nom_categories']?></label>
            </div>
        <?php
            }else{
        ?>
            
        
            <div class="inputCat">
                <input type="radio" class="listCat" name="listCat" value="<?= $idCategories ?>" id="listCat<?= $idCategories ?>">
                <label for="listCat<?= $idCategories ?>" class="catWord"><?= $cat['Nom_categories']?></label>
            </div>
        <?php }
           
        } ?>
	</div>
    <div class="group grpMotCle">
        <h5 class="h5motCle">mot clé <span class="exposant">1 minimum</span></h5>
		<?php
        while ($keyWord = $listKeyWords->fetch())
        {
            ?>
            <div class="inputMotCle">
                <input class="listMotCleArticle" id="motCleArticle<?= $keyWord['id_motCle'] ?>" name="motCleArticle" type="checkbox" value="<?= $keyWord['id_motCle'] ?>"<?php 
                if(1 === preg_match("~[".$keyWord['id_motCle']."]~", $motCle)){
                    ?>checked
                    <?php
                }?> />
                <label for="motCleArticle<?= $keyWord['id_motCle'] ?>" class="KeyWord"><?= $keyWord['Mot_motCle'] ?>
                
                </label>
            </div>
        <?php
        } ?>
	</div>
	<button id="publier" class="group">publier</button>
    <button id="return" class="group annuler">annuler</button>
</div>


<?php $content = ob_get_clean(); ?>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script src="public/js/jquery.min.js"></script>
<script src="public/js/blogAdd.js"></script>
<link rel="stylesheet" type="text/css" href="public/css/blog.css" media="screen"/>
<?php require('view/template.php'); ?>