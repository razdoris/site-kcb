<?php
$title = "KCB-ajouter un article";
ob_start();
?>

<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<div >
	<div class="group">
		<input type='text' placeholder="Titre de l'article" id="title" name="title" />
	</div>
	<div class="group">
		<div id="resume" class="textZone">
		  <p>placer ici un <strong>résumé</strong> de l'article</p>
		  <p><br></p>
		</div>
	</div>
	<div class="group">
		<div id="article" class="textZone">
		  <h3>contenu de l'article</h3>
		  <p>placer ici le <strong>contenu</strong> de l'article</p>
		  <p><br></p>
		</div>
	</div>
    <div class="group grpCategory">
        <h5 class="h5Category">categories </h5>
		<?php
        while ($cat = $listCategories->fetch())
        {
            ?>
            <div class="inputCat">
                <input type="radio" class="listCat" name="listCat" value="<?= $cat['id_categories'] ?>" id="listCat<?= $cat['id_categories'] ?>" >
                <label for="listCat<?= $cat['id_categories'] ?>" class="catWord"><?= $cat['Nom_categories'] ?></label>
            </div>
        <?php
        } ?>
	</div>
    <div class="group grpMotCle">
        <h5 class="h5motCle">mot clé <span class="exposant">1 minimum</span></h5>
		<?php
        while ($keyWord = $listKeyWords->fetch())
        {
            ?>
            <div class="inputMotCle">
                <input class="listMotCleArticle" id="motCleArticle<?= $keyWord['id_motCle'] ?>" name="motCleArticle" type="checkbox" value="<?= $keyWord['id_motCle'] ?>" />
                <label for="motCleArticle<?= $keyWord['id_motCle'] ?>" class="KeyWord"><?= $keyWord['Mot_motCle'] ?></label>
            </div>
        <?php
        } ?>
	</div>
	<button id="publier" class="group">publier</button>
    <button id="return" class="group annuler">annuler</button>
</div>


<?php $content = ob_get_clean(); ?>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script src="public/js/jquery.min.js"></script>
<script src="public/js/blogAdd.js"></script>
<link rel="stylesheet" type="text/css" href="public/css/blog.css" media="screen"/>
<?php require('view/template.php'); ?>