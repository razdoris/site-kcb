<?php $title = "KCB-Presentation" ?>
<?php ob_start(); ?>  
	<section class="chapitre" id="leClub">
        <h2 class="h2View">Presentation du club</h2>
        
        <p>Le Karaté Club de Besné est, comme son nom l'indique, un club de karaté situé sur la commune de besné.</p>
        <p>Il accueille les lundi, mercredi et samedi (en fonction du groupe d'appartenance) des karatekas à partir de 5 an, grâce à sa section Baby.</p>
        <p>De part sa taille modeste, les adherents ne sont regroupés qu'en 4 groupes:</p>
        <ul class="listepres">
            <li class="listeValeur">Babys (de 5ans à 6ans)</li>
            <li class="listeValeur">Enfants loisir (ceinture blanche et jaune et non competiteurs)</li>
            <li class="listeValeur">Enfants Compétition</li>
            <li class="listeValeur">Ado/Adulte</li>
        </ul>
        <p>Bien que le Karaté soit un sport individuel, les membres du bureau souhaitent tout de même insuffler un esprit d'équipe aux adhérents. Ainsi, il n'est pas rare que les plus gradés aident les moins gradés à progresser.</p>
        <p>L'objectif étant que nos adhérents partagent les valeurs du club:</p>
        <ul class="listepres">
            <li class="listeValeur">Respect</li>
            <li class="listeValeur">Combativité</li>
            <li class="listeValeur">Tenacité</li>
            <li class="listeValeur">Humilité</li>
            <li class="listeValeur">Entraide</li>
            <li class="listeValeur">Solidarité</li>
        </ul>
	</section>
    <section class="chapitre" id="adress">
        <h2 class="h2View">Notre adresse</h2>
		<div class="carte"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2700.581923374278!2d-2.0911789843766337!3d47.4005895791715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7cdf623ccb9f54d3!2sKarat%C3%A9%20Club%20de%20Besn%C3%A9!5e0!3m2!1sfr!2sfr!4v1605216845736!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:2;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
        <div class="texteAdresse">
            <div>le club se situe au complexe sportif de Besné:<adress class="realAdress">salle de la fontaine,<br>chemin du stade<br>44160, Besné</adress></div>
            <hr class="hrAdresse">
            <div>mel: <a href="mailto:karateclubdebesne@gmail.com"> karateclubdebesne@gmail.com</a></div>
        </div>
	</section>
    <section class="chapitre" id="horaires">        
        <h2 class="h2View">Les Horaires</h2>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Lundi</th>
                    <th>Mardi</th>
                    <th>Mercredi</th>
                    <th>Jeudi</th>
                    <th>Vendredi</th>
                    <th>Samedi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="2" class="heure">9h45</td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td rowspan="6" class="coursBabys coursKarate"> Babys<br>-6ans<br>Michel Josso</td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">10h00</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">10h15</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">10h30</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td rowspan="12" class="coursTous coursKarate"> Tous Niveaux<br>+6ans<br>Michel Josso</td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">10h45</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">11h00</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">11h15</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">11h30</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">11h45</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">12h00</td>
                </tr>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                </tr>
                <tr class="ligneTableVide">
                    <td colspan="7">...</td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">17h30</td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td><td class="ligneTable"></td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td rowspan="6" class="coursLoisir coursKarate">Enfants Loisirs<br>6 - 14 ans<br>Nicolas Cossais</td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">17h45</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">18h00</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">18h15</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td rowspan="8" class="coursCompetition coursKarate">Enfant Competitions<br>6-14 ans<br>Michel Josso</td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">18h30</td>
                </tr>
                <tr>
                    <td rowspan="12" class="coursAdultes coursKarate">Ado-Adultes<br>+14ans<br>Michel Josso</td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">18h45</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">19h00</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">19h15</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">19h30</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td rowspan="12" class="coursAdultes coursKarate">Ado-Adultes<br>+14ans<br>Michel Josso</td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">19h45</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">20h00</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">20H15</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">20h30</td>
                </tr>
                <tr>
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">20h45</td>
                </tr>
                <tr >
                    <td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td><td class="ligneTable" rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="heure">21h00</td>
                </tr>
                <tr>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                </tr>
            </tbody>
        </table>
          
              
	</section>
    <section class="chapitre" id="bureau">        
        <h2 class="h2View">Le Bureau</h2>
        <div class="ligne">
            <div class="fichIdentit alone">
                <h3>Le president</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du président" /></span>
                <span class="column droite"><h4>Nicolas Beurel</h4><br>1er Kyu<br>3eme Année</span>
            </div>
        </div>
        <div class="ligne">
            <div class="fichIdentit gauche">
                <h3>La secretaire</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo de la secrétaire" /></span>
                <span class="column droite"><h4>Amélie Plays</h4><br>1er Kyu<br>4eme Année</span>
            </div>
            <div class="fichIdentit droite">
                <h3>Le secretaire adjoint</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du secrétaire adjoint" /></span>
                <span class="column droite"><h4>Renan Archaux</h4><br>1er Kyu<br>2eme Année</span>
            </div>
        </div>
        <div class="ligne">
            <div class="fichIdentit gauche">
                <h3>Le trésorier</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du trésorié" /></span>
                <span class="column droite"><h4>Nicolas Cossais</h4><br>1er Dan<br>6eme Année</span>
            </div>
            <div class="fichIdentit droite">
                <h3>Le trésorier adjoint</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du trésorié adjoint" /></span>
                <span class="column droite"><h4>Aurélien Bourdonnec</h4><br>2eme Kyu<br>2eme Année</span>
            </div>
        </div>
	</section>
    <section class="chapitre" id="enseignant">        
        <h2 class="h2View">Les Enseignants</h2>
        <div class="ligne">
            <div class="fichIdentit gauche">
                <h3>Sensei</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du sensei" /></span>
                <span class="column droite"><h4>Michel Josso</h4><br>6eme Dan</span>
            </div>
            <div class="fichIdentit droite">
                <h3>Sensei</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo du sensei" /></span>
                <span class="column droite"><h4>Nicolas Cossais</h4><br>1er Dan</span>
            </div>
        </div>
        <div class="ligne">
            <div class="fichIdentit gauche">
                <h3>Assistant</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo d'un assistant" /></span>
                <span class="column droite"><h4>Amélie Plays</h4><br>1er Kyu</span>
            </div>
            <div class="fichIdentit droite">
                <h3>Assistant</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo d'un assistantg" /></span>
                <span class="column droite"><h4>Nicolas Beurel</h4><br>1er Kyu</span>
            </div>
        </div>
        <div class="ligne">
            <div class="fichIdentit gauche">
                <h3>Assistant</h3>
                <span class="column gauche"><img class="photoIdentit" src="./public/images/kcb.png" title="photo" alt="photo d'un assistant" /></span>
                <span class="column droite"><h4>Renan Archaux</h4><br>1er Kyu</span>
            </div>
        </div>        
	</section>
    <section class="chapitre" id="historique">
        <h2 class="h2View">L'histoire du club</h2>
        <h3 class="h3View">Naissance du KCB</h3>
        <h4  class="h4View">Émergence du projet.</h4>
        <p>Tout commence en 2013 par le désir de Michel Josso (6 ème DAN), entraîneur de karaté et déjà à l’origine de la création de plusieurs clubs dans le département, de fonder son dernier club de karaté. Ce nouveau club doit se construire autour de trois grandes idées :</p>
        <ul>
            <li class="listeValeur">Respect</li>
            <li class="listeValeur">Valeurs</li>
            <li class="listeValeur">Compétition</li>
        </ul>
        <p>Sur les conseils d’Eric Dufour et Franck Biron, adhérents d’un de ses clubs, Michel contacte la Mairie de Besné, afin de savoir si la commune serait partante pour accueillir ce nouveau club.</p> 

        <h4  class="h4View">Création du Club.</h4>

        <p>
            Après l’acceptation de la Mairie, Michel constitue un bureau dans le but de monter un dossier auprès de la Fédération Française de Karaté de l’ensemble des structures officielles, afin d’obtenir les numéros d’agréments nécessaires à la création du club.
        </p>
        <p>
            Le premier bureau du KCB voit le jour et il est constitué de trois membres : Michel Josso au poste de président assisté de Franck Biron et Franck Kerneur respectivement Trésorier et Secrétaire.
        </p>
        <p>
            Christophe Guihard, un quatrième membre non-officiel du bureau, faute de disponibilités, prête "main-forte" à cette petite association lors des réunions et des forums.
        </p>
        
        <h4  class="h4View">Premiers pas…</h4>
        <p>
            C’est ici que le gros travail commence.
        </p>
        <p>
            Franck Kerneur et Michel Josso ouvrent le compte du club, et celui-ci commence son activité; 
        </p>
        <p>
            D’abord 5, puis 10, puis 20 adhérents, jusqu’à atteindre les 30 licenciés…Au fil des mois, le club grandit et se fait d'avantage connaître.
        </p>
        <p>
            Cette année-là, Franck passe son diplôme d’instructeur fédéral, devenant ainsi le second prof du Club.
        </p>
        
        <h3  class="h3View">Le Dojo</h3>
        <p>
            Les premières années, les adhérents s’entraînent dans les salles de sport et gymnases mis à disposition par la commune. Mais en 2015 la rénovation du principal gymnase de Besné est décidé.
        </p>
        <p>
            Le KCB a la chance d’être intégré dans le projet. Une première réunion à laquelle participent Franck Biron et Franck Kerneur lance le projet.
        </p>
        <p>
            Les divers besoins de l’activité du club y sont mentionnés.
        </p>
        <p>
            Les travaux de rénovation prennent une année. Pendant les travaux, pour ne pas perdre d’heures, il faut se déplacer dans diverses salles de la commune.
        </p>
        <p>
           Nicolas Cossais rejoint le bureau pendant les projets de Dojo. A ce moment-là, notre association commence à trouver ses sponsors, à développer au mieux son activité, et augmente ses horaires de cours et ses présences aux diverses réunions.
        </p>
        <p>
            En 2017, souhaitant prendre un peu de recul, Michel Josso céde sa place de dirigeant du club à un ancien élève, Franck Kerneur, mais continue de venir selon ses envies.
        </p>
        <p>
            Pour la saison 2017 -2018, un nouveau bureau est constitué de 4 personnes : Franck Kerneur prend le rôle de président, Nicolas Cossais celui de trésorier tandis que le rôle de secrétaire est assuré par Clément Raveleau et Danielle Cordier.
        </p> 

        <h3 class="h3View">Une Belle longévité.</h3>
        <p>
           Après une saison 2017-2018 difficile et une année 2018-2019 de transition, le club semble avoir trouveé un rythme de croisière.
        </p>
        <p>
            Le dojo lui manquant, Michel revient dans le club en tant que Sensei épaulé par Nicolas Cossais, qui a obtenu son DAF (Diplome d'Animateur Fédéral), et par Amélie Ravelau, Nicolas Beurel et Renan Archaux.
        </p> 
        <p>
           Le club s’organise avec l’âge, trouvant des membres qui s’investissent et le font grandir. Ainsi le bureau version 2020 est composé de 5 personnes (cf page <a href="index.php?view=presentation#bureau">le bureau</a>).
        </p>
        <p>
            Durant toutes ces années le club a mené à bien plusieurs de ses projets: obtenir un véritable dojo, équiper ses adhérents, fonder un groupe de Baby karaté, obtenir des sponsors et s’impliquer un peu auprès de la jeunesse en sont des exemples. Mais le club ne compte pas s'arrêter en si bon chemin...
        </p> 

        <p>
           L’histoire continue, pourquoi ne pas en faire parti...
        </p>
        <p>
            Pour ça, rien de plus simple. Vous voulez découvrir le club?
            Qui peut en parler le mieux? ...<br>
            ...Ses adhérents bien-sûr !!! Rencontrez-les, échangez avec eux ; ils ne sont pas violents !!!.
        </p>
      
        
	</section>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/presentation.css" media="screen"/>

<?php require('template.php'); ?>