<?php

require_once('model/blog.php');

function accueil()
{
    require('view/accueilView.php');
}

function quiz()
{
    require('view/jeux/quizView.php');
}


function parades()
{
    require('view/jeux/paradesView.php');
}

function presentation()
{
    require('view/presentationView.php');
}


function ceinture($colorBelt)
{
    require('view/lecon/ceintureView.php');
}

function alphabet()
{
    require('view/lecon/abckcbView.php');
}

function sinscrire()
{
    require('view/sinscrireView.php');    
}

function inscription()
{
    require('view/inscriptionView.php');    
}

function courVideo()
{
    require('view/lecon/videoView.php');
}

function blog()
{
    $getterBlog = new GetterBlog();
    $idArticle = '%';
    $motCle = '%';
    $idcategorie ='%';
    $listArticle = $getterBlog->readArticles($idArticle, $motCle, $idcategorie);
    $listCategories = $getterBlog->readCategorie();
    $order = "aleatoire";
    $listKeyWords = $getterBlog->readKeyWords($order);
    require('view/blog/blogView.php');
}

function article($idArticle)
{
    $getterBlog = new GetterBlog();
    $idArticle = $idArticle;    
    $motCle = '%';
    $idcategorie ='%';
    $listArticle = $getterBlog->readArticles($idArticle, $motCle, $idcategorie);
    $Commentaires = $getterBlog->readCommentaires($idArticle);
    require('view/blog/articleView.php');
}

function addArticle()
{
    $getterBlog = new GetterBlog();
    $order = "id";
    $listKeyWords = $getterBlog->readKeyWords($order);
    $listCategories = $getterBlog->readCategorie();
    require('view/blog/newArticleView.php');
}


function modifArticle($idArticle)
{
    $getterBlog = new GetterBlog();
    $idArticle = $idArticle;    
    $motCle = '%';
    $idcategorie ='%';
    $listArticle = $getterBlog->readArticles($idArticle, $motCle, $idcategorie);
    $order = "id";
    $listKeyWords = $getterBlog->readKeyWords($order);
    $listCategories = $getterBlog->readCategorie();
    require('view/blog/modifArticleView.php');
}

function error($errorMsg)
{
    require('view/error.php');
}

function login($msg, $currentView)
{
    require('view/login.php');
}



