$(function() {
    
    $('.orange, .vert, .bleu, .marron').addClass('barre');
    
    initColor();
    
    $('#choixCeinture').change(function(){
        initColor();
    });
    
    function initColor(){
        $('.orange, .vert, .bleu, .marron').removeClass('barre');
        color=$('#choixCeinture').val();
        switch (color) {
            case 'jaune':
                $('#choixCeinture').css("background-color","#FFFE54");
                $('.orange, .vert, .bleu, .marron').addClass('barre');
                break;
            case 'orange':
                $('#choixCeinture').css("background-color","#EE833F");
                $('.vert, .bleu, .marron').addClass('barre');
                break;
            case 'vert':
                $('#choixCeinture').css("background-color","#31B82E");
                $('.bleu, .marron').addClass('barre');
                break;
            case 'bleu':
                $('#choixCeinture').css("background-color","#5181FF");
                $('.marron').addClass('barre');
                break;
            case 'marron':
                $('#choixCeinture').css("background-color","brown");
                break;
                
        }
        $(".imageProg").attr("src","./public/images/cours/"+color+".jpg")
    }
});