jQuery(function() {
    
    $(".parentalAdvisory").hide();
    
    $("#fieldNewKarate0").click(function(){
        $("#fieldNumLicence, #fieldCompetition1, .fieldCompetition, #fieldGrade").prop('disabled', true);
        $("#fieldCompetition0").prop('checked',true);
     })
    
    $("#fieldNewKarate1").click(function(){
        $("#fieldNumLicence, #fieldCompetition1, .fieldCompetition, #fieldGrade").prop('disabled', false);
        $("#fieldCompetition0").prop('checked',false);
     })
    
    $("#fieldCompetition0").click(function(){
        $(".fieldCompetition").prop('disabled', true);
    });
    
    $("#fieldCompetition1").click(function(){
        $(".fieldCompetition").prop('disabled', false);
    });
    
    $("#fieldBirthday").change(function(){
        birthday = $("#fieldBirthday").val();
        birthday = new Date(birthday);
        today = new Date();
		age = parseFloat(Math.floor((today - birthday)/(365.25 * 24 * 60 * 60 *1000)));
        console.log(today + " " + birthday +" " +age);
        if(age<18){
            $(".parentalAdvisory").show();
            $(".fieldTransport").attr("required", "required");
            $(".fieldGroupeParentalAutorisation, .fieldGroupTransportation").addClass("fieldRequired");
        } else if(age>18){
            $(".parentalAdvisory").hide();
            $(".fieldTransport").removeAttr("required");
            $(".fieldGroupeParentalAutorisation, .fieldGroupTransportation").removeClass("fieldRequired");
        }
        year = birthday.getFullYear();
        console.log(year);
        $('option:contains("'+year+'")').prop('selected', true);
    })
});