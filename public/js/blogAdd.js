$(function() {
	
  var toolbarOptions = [
	  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
	  ['blockquote', 'code-block'],

	  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],          
	  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
	  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
	  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
	  
	  [{ 'size': [] }],  // custom dropdown
	  

	  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
	  [{ 'font': [] }],
	  [{ 'align': [] }],
	  ['image','video','formula', 'link'],
	  
	  ['clean']                                         // remove formatting button
	];
	
	var toolbarOptions2 = [
	  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
	  ['blockquote', 'code-block'],
         
	  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
	  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
	  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
	  
	  [{ 'size': [] }],  // custom dropdown
	  

	  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
	  [{ 'font': [] }],
	  [{ 'align': [] }],
	  
	  ['clean']                                         // remove formatting button
	];

	var quillResume = new Quill('#resume', {
	  modules: {
		toolbar: toolbarOptions2
	  },
	  theme: 'snow'
	});

	var quill = new Quill('#article', {
  theme: 'snow',
  modules: {
    toolbar: {
      container: [
        [{
          font: []
        }],
        [{
          header: [false, 1, 2, 3, 4, 5, 6]
        }],
        ["bold", "italic", "underline", "strike"],
        [{
          align: []
        }],
        ["blockquote", "code-block"],
        [{
          list: "ordered"
        }, {
          list: "bullet"
        }, {
          list: "check"
        }],
        [{
          indent: "-1"
        }, {
          indent: "+1"
        }],
        [{
          color: []
        }, {
          background: []
        }],
        ["link", "image", "video"],
        ["clean"],
        ["showHtml"]
      ],
      handlers: {
        showHtml: () => {
          if (quill.txtArea.style.display === "") {
            const html = quill.txtArea.value;
            if (html === '<p><br/></p>') {
              quill.html = null;
            } else {
              quill.html = html.replace(new RegExp('<p><br/>', 'g'), '<p>')
            }
            quill.pasteHTML(html);
          }
          quill.txtArea.style.display =
            quill.txtArea.style.display === "none" ? "" : "none";
        }
      }
    }
  }
});
    
    var quillCommentaire = new Quill('#commentaire', {
	  modules: {
		toolbar: toolbarOptions2
	  },
	  theme: 'snow'
	});
    
    
    
if (Quill.prototype.getHTML == undefined)
  Quill.prototype.getHTML = function() {
    return this.container.firstChild.innerHTML;
  };

//fix for Quill v2
if (Quill.prototype.pasteHTML == undefined)
  Quill.prototype.pasteHTML = function(html) {
    {
      this.container.firstChild.innerHTML = html;
    }
  }

quill.txtArea = document.createElement("textarea");
quill.txtArea.style.cssText =
  "width: 100%;margin: 0px;background: rgb(29, 29, 29);box-sizing: border-box;color: rgb(204, 204, 204);font-size: 15px;outline: none;padding: 20px;line-height: 24px;font-family: Consolas, Menlo, Monaco, &quot;Courier New&quot;, monospace;position: absolute;top: 0;bottom: 0;border: none;display:none;resize: none;";

var htmlEditor = quill.addContainer("ql-custom");
htmlEditor.appendChild(quill.txtArea);

quill.on("text-change", (delta, oldDelta, source) => {  
  var html = quill.getHTML();
  quill.txtArea.value = html;
});

quill.txtArea.value = quill.getHTML();
    
    
	$('#publier').click(function(){
		
        verifMotCle = $('.listMotCleArticle:checked').length;
        console.log(verifMotCle);
        if(verifMotCle == 0)
            {
                alert("ERREUR \n Merci de sélectionner au moins un mot clé");
                return;
            }
		titleArticle = $("#title").val();
		resumeArticle = quillResume.root.innerHTML;
		contenuArticle = quill.root.innerHTML;
        categorieArticle = $(".listCat").attr('checked',true).val();
		actionAFaire = 'addArticle';
		console.log(titleArticle);
		console.log(resumeArticle);
		console.log(contenuArticle);
        console.log("categorie: "+categorieArticle);
		$.post('public/ajax/gestionBlog.php', { action: actionAFaire, title: titleArticle, resume: resumeArticle, contenu: contenuArticle, categorie: categorieArticle }, function(reponse){
			console.log("requette effectué \n" + reponse);
            idArticle = reponse;
            $('.listMotCleArticle').each(function(){
            if($(this).is(":checked")){
                idKeyWord = $(this).val();                
                actionAFaire = 'addKeyWord';
                $.post('public/ajax/gestionBlog.php', { action: actionAFaire, idKeyWord: idKeyWord, idArticle: idArticle}, function(reponse){
                    console.log("requette effectué \n" + reponse);
                    alert(reponse);
                    
                });
            }
        })
		});
        
	  });
    
    $('#annuler').click(function(){
        location.reload(true);
    });
    
    $('#return').click(function(){
        history.back();
    });
    
    $('#soumettre').click(function(e){
        e.preventDefault();
        nomCommentateur = $('#nomCommentateur').val();
        idArticleComent = $('#idArticle').val();
        commentaire = quillCommentaire.root.innerHTML;
        actionAFaire = 'addComment';
        console.log(idArticleComent + ' ' + nomCommentateur + ' ' + actionAFaire);
        $.post('public/ajax/gestionBlog.php', { action: actionAFaire, nomComent: nomCommentateur, idArticle: idArticleComent, contenu: commentaire }, 
    
        function(reponse){
			console.log("requette effectué \n" + reponse);
		});
        
    });
    
    
    $('.oneKeyWord').click(function(){
        console.log($(this).val());
        if($(this).hasClass('selected'))
        {
            idKeyWord ='%';
            $(this).removeClass('selected');
        } else{
            $('.selected').removeClass('selected');
            $(this).addClass('selected');
            idKeyWord = $(this).next('.idKeyWord').html();
            console.log("mot cle: " + idKeyWord);
            actionAFaire = 'modifIteration';
            $.post('public/ajax/gestionBlog.php', { action: actionAFaire, idKeyWord: idKeyWord}, function(reponse){
                console.log("requette effectué \n" + reponse);
            });
        }
        console.log(idKeyWord);        
        actionAFaire = 'triMotCle';
        $.post('public/ajax/gestionBlog.php', { action: actionAFaire, idKeyWord: idKeyWord}, function(reponse){
			console.log("requette effectué \n" + reponse);
            $('#listArticle').html(reponse);
        });
    });
    
    
    $('.category').change(function(e){
        e.preventDefault();
        idCatArticle = $('#selectCat').val();
        if(idCatArticle == 0){
            idCatArticle = '%'
        }
        actionAFaire = 'triCategorie';
        $.post('public/ajax/gestionBlog.php', { action: actionAFaire, idCatArticle: idCatArticle}, function(reponse){
			console.log("requette effectué \n" + reponse);
            $('#listArticle').html(reponse);
        });
    });
    

 });
