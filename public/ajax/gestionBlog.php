<?php

require_once('../../model/blog.php');


$dateDuJour = date("Y-m-d H:i:s");

$setterBlog = new SetterBlog();
$getterBlog = new GetterBlog();
$action = $_POST['action'];

if($action == "addArticle")
{
    $titleArticle = $_POST['title'];
    $resumeArticle = $_POST['resume'];
    $contenuArticle = $_POST['contenu'];
    $categorieArticle = $_POST['categorie'];
    
    try
    {
        $addArticle = $setterBlog->addArticleBlog($titleArticle, $resumeArticle, $contenuArticle, $categorieArticle, $dateDuJour);
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }  
    
    $lastArticle = $getterBlog->getLastArticle();
    $article = $lastArticle->fetch();
    $idArticle = $article['max_id_article'];
    

    echo $idArticle;
   
}elseif($action == "addComment"){
    
    $nomComentateur = $_POST['nomComent'];
    $idArticleCom = $_POST['idArticle'];    
    $contenuCommentaire = $_POST['contenu'];
        
    try
    {
        $addCommentaire = $setterBlog->addCommentaireBlog($idArticleCom, $nomComentateur, $contenuCommentaire, $dateDuJour);
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    } 

    echo 'commentaire ajouté';
    
    
}elseif($action == "triMotCle"){
    
    $idMotCle = $_POST['idKeyWord'];
    $idCategorie = '%';
    $idArticle = '%';
    
    
    try
    {
        $listArticle = $getterBlog->readArticles($idArticle, $idMotCle, $idCategorie);
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    } 
    
    $listDesArticle="";
    
    while ($article = $listArticle->fetch())
    {
       $listDesArticle .= "<a class='articleContent' href='./index.php?view=article&idArticle=" . $article['id_article'] . "'>
            <h3 class='h3View'>" . $article['Title_article'] . "</h3>
            <div class='resumeArticle'>" . $article['Resume_article']. "</div>
        </a>";
        
    }
    
    /*$setterBlog->modifUpIteration($idMotCle);*/
        
    echo $listDesArticle;
    
    
}elseif($action == "triCategorie"){
    
    $idCategorie = $_POST['idCatArticle'];
    $idMotCle = '%';
    $idArticle = '%';
    
    
    try
    {
        $listArticle = $getterBlog->readArticles($idArticle, $idMotCle, $idCategorie);
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    } 
    
    $listDesArticle="";
    
    
    while ($article = $listArticle->fetch())
    {
       $listDesArticle .=  "<a class='articleContent' href='./index.php?view=article&idArticle=" . $article['id_article'] . "'>
            <h3 class='h3View'>" . $article['Title_article'] . "</h3>
            <div class='resumeArticle'>" . $article['Resume_article']. "</div>
        </a>" ;
        
    }
    
    /*$setterBlog->modifUpIteration($idMotCle);*/
        
    echo $listDesArticle;
    
    
}elseif($action == "addKeyWord"){
    $idMotCle = $_POST['idKeyWord'];
    $idArticle = $_POST['idArticle'];
    $setterBlog->addKeyWord($idMotCle, $idArticle);
    echo 'mot cle ajouté';
}
elseif($action == "modifIteration"){
    $idMotCle = $_POST['idKeyWord'];
    $setterBlog->modifUpIteration($idMotCle);
    $setterBlog->modifDownIteration($idMotCle);
    echo 'nbr changé';
}

