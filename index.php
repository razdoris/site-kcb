<?php
session_start();
require('controller/frontend.php');
require_once('model/conf.php');


if(isset($_POST['login']) AND (!empty($_POST['login'])))
{
	$secret = G_SECRET_KEY;
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success)
    {
        $login = $_POST['login'];
        $pwd = $_POST['pwd'];
        $getterUser = new GetterUser();
        $loginPassUsers = $getterUser->checkIdentifiant($login);
        $loginPassUser = $loginPassUsers->fetch();
        $loginUser = $loginPassUser['Login_user'];
        $passUser = $loginPassUser['Pass_user'];

        if(password_verify($pwd,$passUser))
        {
            $_SESSION['login'] = $login;
            $msg = "allRight";
        }else{	
            $msg='identifiant et/ou mot de passe erroné';	
            //login($msg);	
        }
    }else{
       $msg= 'bot validation échoué';
	   //login($msg);
    }
}elseif(isset($_SESSION['login']) AND (!empty($_SESSION['login']))){		
        $msg = "allRight";
}else{ 
	$msg= 'vous devez etre connecté';
	//login($msg);		  
}







if(isset($_GET['view'])){
	if($_GET['view'] == 'accueil'){
		accueil();
	}elseif($_GET['view'] == 'quiz'){
		quiz();
    }elseif($_GET['view'] == 'parades'){
		parades();
        
	}elseif($_GET['view'] == 'abckcb'){
		alphabet();
	}elseif($_GET['view'] == 'presentation'){
		presentation();
	}elseif($_GET['view'] == 'video'){
		courVideo();
	}elseif($_GET['view'] == 'blog'){
		blog();
	}elseif($_GET['view'] == 'addArticle'){
		if(isset($_SESSION['login']))
        {
            addArticle();
        }else{
            $currentView='addArticle';
            login($msg, $currentView);
        };
	}elseif($_GET['view'] == 'modifArticle'){
		if(isset($_SESSION['login']))
        {
            $idArticle = $_GET['idArticle'];
            modifArticle($idArticle);
        }else{
            $currentView='blog';
            login($msg, $currentView);
        };
	}elseif($_GET['view'] == 'article'){
        if((isset($_GET['idArticle'])) AND (!empty($_GET['idArticle']))) {
            $idArticle = $_GET['idArticle'];
            article($idArticle);
        }else{
            $errorMsg = "cette article n'existe pas <br> <a href='./index.php?view=blog' >retour au blog</a>";
            error($errorMsg);
        }
	}elseif($_GET['view'] == 'sinscrire'){
		sinscrire();
	}elseif($_GET['view'] == 'inscription'){
        if(isset($_SESSION['login']))
        {
            inscription();
        }else{
            $currentView='inscription';
            login($msg, $currentView);
        }
	}elseif($_GET['view'] == 'ceinture'){
        $colorBelt = $_GET['color'];
		ceinture($colorBelt);
	}elseif($_GET['view'] == 'login'){
        $msg = 'essai';
        $currentView='accueil';
		login($msg, $currentView);
	}elseif($_GET['view'] == 'logout'){
        $_SESSION['login'] = "";;
		$msg = 'vous êtes déconnecté';
        $currentView='accueil';
		login($msg, $currentView);
	}
}else{
	accueil();
}

