<?php

require("conf.php");

class blog
{
    protected function dbconnect()
	{
        $host = DB_HOST;
        $dataBaseName = DB_NAME; 
        $user = DB_USER;
        $pass = DB_PASS;


        try
        {

            $db = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName , $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $db;
    }
}

class GetterBlog extends blog
{
    function readArticles($idArticle, $motCle, $idcategorie)
    {
        $db = $this->dbconnect(); 
        
       
        
        $testTotal = $idArticle ." - " . $motCle  ." - " . $idcategorie;
        try
        {
            $listArticles = $db->prepare("SELECT `kcb_blog_articles`.*, 
                                            GROUP_CONCAT(DISTINCT kcb_blog_motcle.id_motCle)As grp_id_motCle
                                            FROM `kcb_blog_articles` 
                                            LEFT JOIN `kcb_blog_article-motcle` ON `kcb_blog_article-motcle`.`id_article_Article-MotCle` = `kcb_blog_articles`.`id_article` 
                                            LEFT JOIN `kcb_blog_motcle` ON `kcb_blog_article-motcle`.`id_motCle_Article-MotCle` = `kcb_blog_motcle`.`id_motCle`
                                            WHERE kcb_blog_motcle.id_motCle LIKE :motCle
                                            AND kcb_blog_articles.id_article LIKE :idArticle
                                            AND kcb_blog_articles.id_categories_articles LIKE :idCatArticle
                                            GROUP BY `kcb_blog_articles`.id_article
                                            ORDER BY id_article DESC");
            $listArticles->execute(array('idArticle' => $idArticle,
                                        'motCle' => $motCle,
                                        'idCatArticle' => $idcategorie));

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $listArticles;
        $listArticles->closeCursor();
        
    }
    
    function readCategorie()
    {
        $db = $this->dbconnect(); 
        try
        {
            $listcategories = $db->query("SELECT *
                                            FROM `kcb_blog_categories`
                                            WHERE 1");

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $listcategories;
        $listcategories->closeCursor();
        
    }
    /*function readArticle($idArticle)
    {
        $db = $this->dbconnect(); 
        try
        {
            $article = $db->prepare("SELECT `kcb_blog_articles`.* 
                                            FROM `kcb_blog_articles`
                                            WHERE kcb_blog_motcle.Mot_motCle LIKE :idArticle");
            $article->execute(array('idArticle' => $idArticle,
                                        'motCle' => $motCle));

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $article;
        $article->closeCursor();
        
    }*/
    
    function readCommentaires($idArticle)
    {
        $db = $this->dbconnect(); 
        try
        {
            $listCommentaires = $db->prepare("SELECT * 
                                            FROM kcb_blog_commentaires 
                                            WHERE kcb_blog_commentaires.id_article_commentaire LIKE :idArticle");
            $listCommentaires->execute(array('idArticle' => $idArticle));

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $listCommentaires;
        $listCommentaires->closeCursor();
        
    }
    
    function readKeyWords($order)
    {
        
        if($order == "aleatoire")
        {
            $sort = 'RAND()';
        }else{
            $sort = 'kcb_blog_motcle.id_motCle ASC';
        }
        $db = $this->dbconnect(); 
        try
        {
            $listKeyWords = $db->query("SELECT * 
                                            FROM `kcb_blog_motcle`
                                            WHERE 1
                                            ORDER BY " . $sort);

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $listKeyWords;
        $listKeyWords->closeCursor();
    }
    
    function getLastArticle()
    {
        $db = $this->dbconnect(); 
        try
        {
            $lastArticle = $db->query("SELECT max(id_article) 
                                            as max_id_article FROM kcb_blog_articles");

        }
        catch (Exception $e)
        {
            die('erreur : ' . $e->getmessage());
        }
        
        return $lastArticle;
        $lastArticle->closeCursor();
    }

}


class SetterBlog extends blog
{
    function addArticleBlog($titleArticle, $resumeArticle, $contenuArticle, $categorieArticle,  $dateDuJour)
    {
        $db = $this->dbconnect();
        
                
        $newArticle = $db->prepare("INSERT INTO kcb_blog_articles (
                                        id_categories_articles,
                                        Title_article,
                                        Resume_article,
                                        Contenu_article,
                                        Date_article)
                                        VALUES (
                                        :categorie,
                                        :title,
                                        :resume,
                                        :contenu,
                                        :date
                                        )");
        $newArticle->execute(array('categorie' => $categorieArticle,
                                   'title' => $titleArticle,
                                    'resume' => $resumeArticle,
                                    'contenu' => $contenuArticle,
                                    'date' => $dateDuJour
                                        ));

        
        
        $newArticle->closeCursor();
    }
    
    function addCommentaireBlog($idArticleCom, $nomComentateur, $contenuCommentaire, $dateDuJour)
    {
        $db = $this->dbconnect();
        
                
        $newComment = $db->prepare("INSERT INTO kcb_blog_commentaires (
                                        id_article_commentaire,
                                        Auteur_commentaires,
                                        Corps_commentaires,
                                        Date_commentaires)
                                        VALUES (
                                        :idArticle,
                                        :auteur,
                                        :corps,
                                        :date
                                        )");
        $newComment->execute(array('idArticle' => $idArticleCom,
                                    'auteur' => $nomComentateur,
                                    'corps' => $contenuCommentaire,
                                    'date' => $dateDuJour
                                        ));

        
        
        $newComment->closeCursor();
    }
    
    
    function modifUpIteration($idMotCle)
    {
        $db = $this->dbconnect();
                
        
        $upIteration = $db->prepare("UPDATE kcb_blog_motcle 
                                    SET Iteration_motcle = if(Iteration_motcle+2>36,36,Iteration_motcle+2) 
                                    WHERE id_motCle = :idkeyword");
        $upIteration->execute(array('idkeyword' => $idMotCle));

        $upIteration->closeCursor();
    }
    
    
    function modifDownIteration($idMotCle)
    {
        $db = $this->dbconnect();
        
        $downIteration = $db->prepare("UPDATE kcb_blog_motcle 
                                    SET Iteration_motcle = if(Iteration_motcle-2<10,10,Iteration_motcle-2) 
                                    WHERE id_motCle <> :idkeyword");
        $downIteration->execute(array('idkeyword' => $idMotCle));

        $downIteration->closeCursor();
        
    }
    
    function addKeyWord($idMotCle, $idArticle)
    {
        $db = $this->dbconnect();
        $addKeyWord = $db->prepare("INSERT INTO `kcb_blog_article-motcle` 
                                    (`id_article_Article-MotCle`, `id_motCle_Article-MotCle`)
                                    VALUES (:idArticle, :idkeyword);");
        $addKeyWord->execute(array('idArticle' => $idArticle,
                                      'idkeyword' => $idMotCle));
        
        $addKeyWord->closeCursor();
    }
    
}
    
class GetterUser extends blog
{
    public function checkIdentifiant($login)
	{
		$db = $this->dbconnect();
		$ligneIdentifiant = $db->prepare(
						'SELECT kcb_user_acces.Login_user, 
                        kcb_user_acces.Pass_user
						FROM kcb_user_acces
						WHERE kcb_user_acces.Login_user LIKE :login');
		$ligneIdentifiant->execute(array('login' => $login));
		return $ligneIdentifiant;
		$ligneIdentifiant->closeCursor();
	}
}
